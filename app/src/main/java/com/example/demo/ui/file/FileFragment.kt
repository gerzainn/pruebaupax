package com.example.demo.ui.file

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.demo.R
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import java.util.*


class FileFragment : Fragment() {

    private val PICK_IMAGE_REQUEST = 71
    private var filePath: Uri? = null
    private var firebaseStore: FirebaseStorage? = null
    private var storageReference: StorageReference? = null


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_file, container, false)

        firebaseStore = FirebaseStorage.getInstance()
        storageReference = FirebaseStorage.getInstance().getReference("Uploads")

        val filechose: Button = root.findViewById(R.id.btn_choose_image)

        filechose.setOnClickListener{
            launchGallery()
        }

        val upload : Button = root.findViewById(R.id.btn_upload_image)

        upload.setOnClickListener { uploadImage() }
        return root

    }


    private fun launchGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    private fun uploadImage(){

        val progress = ProgressDialog(requireContext()).apply {
            setTitle("Cargando")
            setCancelable(true)
            setCanceledOnTouchOutside(false)
            show()
        }
        var value = 0.0
        var mReference = filePath?.lastPathSegment?.let { storageReference?.child(it) }

        try {
            filePath?.let {
                mReference?.putFile(it)?.addOnSuccessListener { taskSnapshot: UploadTask.TaskSnapshot? ->
                    var url = taskSnapshot!!.metadata

                }
            }

            filePath.let {
                it?.let { it1 ->
                    mReference?.putFile(it1)?.addOnProgressListener { taskSnapshot ->
                        value = (100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount
                        Log.v("value","value=="+value)
                        progress.setMessage("Porcentaje " + value.toInt() + "%")
                        if(value == 100.00)
                        {
                            progress.dismiss()
                        }

                    }
                }
            }


        }catch (e: Exception) {
           Log.e("FileFragment",e.localizedMessage)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if(data == null || data.data == null){
                return
            }

            when (requestCode){
                PICK_IMAGE_REQUEST -> {
                    filePath = data!!.getData()
                    uploadImage()

                }
            }
        }
    }
}