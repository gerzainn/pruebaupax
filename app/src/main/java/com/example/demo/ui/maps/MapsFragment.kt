package com.example.demo.ui.maps

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.demo.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsFragment : Fragment() {

    private lateinit var fusedLocClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap
    private val callback = OnMapReadyCallback { googleMap ->

        map = googleMap

        setupLocClient()
        getCurrentLocation()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_maps2, container, false)


    }

    companion object {
        private const val REQUEST_LOCATION = 1 //request code to identify specific permission request
        private const val TAG = "MapsActivity" // for debugging
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun setupLocClient() {
        fusedLocClient =
                activity?.let { LocationServices.getFusedLocationProviderClient(it) }!!
    }

    private fun getCurrentLocation() {
        if (activity?.let {
                    ActivityCompat.checkSelfPermission(it,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                } !=
                PackageManager.PERMISSION_GRANTED) {

        } else {

            fusedLocClient.lastLocation.addOnCompleteListener {

                val location = it.result


                if (location != null) {

                    val latLng = LatLng(location.latitude, location.longitude)

                    map.addMarker(MarkerOptions().position(latLng)
                            .title("Posicion actual"))
                    val update = CameraUpdateFactory.newLatLngZoom(latLng, 16.0f)

                    map.moveCamera(update)


                } else {

                    Log.e(TAG, "No location found")
                }



            }
        }
    }


}